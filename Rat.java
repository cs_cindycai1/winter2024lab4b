public class Rat{
	private String name;
	private String favorFood;
	private double age;
	
	public Rat(String name, String favorFood, double age) {
		this.name = name;
		this.favorFood = favorFood;
		this.age = age;
	}
	
	//custom instance methos
	public void eat(){
		System.out.println(this.name+" is eating "+this.favorFood+". "+this.name+" like "+this.favorFood+" so much!");
	}
	public void sleep(){
		System.out.println(this.name+": ZZZZzzzz");
	}

	//setters
	public void setAge(double age) {
		this.age = age;
	}
	
	//getters
	public String getName() {
		return this.name;
	}
	
	public String getFavorFood() {
		return this.favorFood;
	}
	
	public double getAge() {
		return this.age;
	}
}
import java.util.Scanner;
public class VirtualPetApp{
	public static void main(String[]args){
		Rat[] mischief=new Rat[4];
		Scanner reader=new Scanner(System.in);
		
		
		for(int i=0; i<mischief.length; i++){
			System.out.println("Choose a name for your rat "+(i+1) );
			String name = reader.next();
			System.out.println("Choose the favorite food for your rat "+(i+1) );
			String favorFood = reader.next();
			System.out.println("Choose the age(no more than 4 year) for your rat "+(i+1) );
			double age = reader.nextDouble();
			
			mischief[i]=new Rat(name, favorFood, age);
		}
		
		System.out.println(mischief[mischief.length - 1].getName());
		System.out.println(mischief[mischief.length - 1].getFavorFood());
		System.out.println(mischief[mischief.length - 1].getAge());
		
		System.out.println("Choose the age(no more than 4 year) for your rat "+ (mischief.length - 1) );
		mischief[mischief.length - 1].setAge(reader.nextDouble());
		
		//test results
		System.out.println(mischief[mischief.length - 1].getName());
		System.out.println(mischief[mischief.length - 1].getFavorFood());
		System.out.println(mischief[mischief.length - 1].getAge());
	}
}